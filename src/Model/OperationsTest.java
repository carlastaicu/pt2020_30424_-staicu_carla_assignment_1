package Model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import junit.framework.TestCase;
class OperationsTest extends TestCase {

    Polynomial firstPolynomial = new Polynomial();
    Polynomial secondPolynomial = new Polynomial();
    @Test
    void addition() throws Exception{
        firstPolynomial.addMonomial(new Monomial(2, 3));
        secondPolynomial.addMonomial(new Monomial(1, 0));

        Polynomial result = new Polynomial();
        result.addMonomial(new Monomial(2, 3));
        result.addMonomial(new Monomial(1, 0));
        result.setDegree();

        Polynomial addition = PolyModel.addition(firstPolynomial, secondPolynomial);

        assertEquals(addition.getArrMonoms().get(0).getCoefficient(), result.getArrMonoms().get(0).getCoefficient());
        assertEquals(addition.getArrMonoms().get(0).getExponent(), result.getArrMonoms().get(0).getExponent());
        assertEquals(addition.getArrMonoms().get(1).getCoefficient(), result.getArrMonoms().get(1).getCoefficient());
        assertEquals(addition.getArrMonoms().get(1).getExponent(), result.getArrMonoms().get(1).getExponent());
        assertEquals(addition.getDegree(), result.getDegree());
    }

    @Test
    void subtraction() {
        firstPolynomial.addMonomial(new Monomial(2, 3));
        secondPolynomial.addMonomial(new Monomial(1, 0));

        Polynomial result = new Polynomial();
        result.addMonomial(new Monomial(2, 3));
        result.addMonomial(new Monomial(-1, 0));
        result.setDegree();

        Polynomial subtraction = PolyModel.subtraction(firstPolynomial, secondPolynomial);

        assertEquals(subtraction.getArrMonoms().get(0).getCoefficient(), result.getArrMonoms().get(0).getCoefficient());
        assertEquals(subtraction.getArrMonoms().get(0).getExponent(), result.getArrMonoms().get(0).getExponent());
        assertEquals(subtraction.getArrMonoms().get(1).getCoefficient(), result.getArrMonoms().get(1).getCoefficient());
        assertEquals(subtraction.getArrMonoms().get(1).getExponent(), result.getArrMonoms().get(1).getExponent());
        assertEquals(subtraction.getDegree(), result.getDegree());
    }

    @Test
    void multiplication() {
        firstPolynomial.addMonomial(new Monomial(1, 1));
        firstPolynomial.addMonomial(new Monomial(-1, 0)); //x-1

        secondPolynomial.addMonomial(new Monomial(1, 1));
        secondPolynomial.addMonomial(new Monomial(1, 0)); //x+1
        Polynomial multiplication = PolyModel.multiplication(firstPolynomial, secondPolynomial); // x^2-1

        Polynomial result = new Polynomial();
        result.addMonomial(new Monomial(1, 2));
        result.addMonomial(new Monomial(-1, 0));
        result.setDegree(); //x^2-1

        assertEquals(multiplication.getArrMonoms().get(0).getCoefficient(), result.getArrMonoms().get(0).getCoefficient());
        assertEquals(multiplication.getArrMonoms().get(0).getExponent(), result.getArrMonoms().get(0).getExponent());
        assertEquals(multiplication.getArrMonoms().get(1).getCoefficient(), result.getArrMonoms().get(1).getCoefficient());
        assertEquals(multiplication.getArrMonoms().get(1).getExponent(), result.getArrMonoms().get(1).getExponent());
        assertEquals(multiplication.getDegree(), result.getDegree());

    }

    @Test
    void division() {
        firstPolynomial.addMonomial(new Monomial(2, 3));
        firstPolynomial.addMonomial(new Monomial(3, 2));
        firstPolynomial.addMonomial(new Monomial(-1, 1));
        firstPolynomial.addMonomial(new Monomial(5, 0)); //2x^3+3x^2-x+5

        secondPolynomial.addMonomial(new Monomial(1, 2));
        secondPolynomial.addMonomial(new Monomial(-1, 1));
        secondPolynomial.addMonomial(new Monomial(1, 0));
        Polynomial[] division = PolyModel.division(firstPolynomial, secondPolynomial); // x^2-x+1

        Polynomial result = new Polynomial();
        result.addMonomial(new Monomial(2, 1));
        result.addMonomial(new Monomial(5, 0));
        result.setDegree(); //x^2-1

        Polynomial remainder = new Polynomial();
        remainder.addMonomial(new Monomial(2, 1));
        remainder.setDegree(); //2x

        assertEquals(division[0].getArrMonoms().get(0).getCoefficient(), result.getArrMonoms().get(0).getCoefficient());
        assertEquals(division[0].getArrMonoms().get(0).getExponent(), result.getArrMonoms().get(0).getExponent());
        assertEquals(division[0].getArrMonoms().get(1).getCoefficient(), result.getArrMonoms().get(1).getCoefficient());
        assertEquals(division[0].getArrMonoms().get(1).getExponent(), result.getArrMonoms().get(1).getExponent());
        assertEquals(division[0].getDegree(), result.getDegree());


        assertEquals(division[1].getArrMonoms().get(0).getCoefficient(), remainder.getArrMonoms().get(0).getCoefficient());
        assertEquals(division[1].getArrMonoms().get(0).getExponent(), remainder.getArrMonoms().get(0).getExponent());
        assertEquals(division[1].getDegree(), result.getDegree());
    }

    @Test
    void derive() {
        firstPolynomial.addMonomial(new Monomial(2, 3));
        firstPolynomial.setDegree();

        Polynomial result = new Polynomial(new Monomial(6, 2));
        Polynomial derive = PolyModel.derive(firstPolynomial);

        assertEquals(derive.getArrMonoms().get(0).getCoefficient(), result.getArrMonoms().get(0).getCoefficient());
        assertEquals(derive.getArrMonoms().get(0).getExponent(), result.getArrMonoms().get(0).getExponent());
        assertEquals(derive.getDegree(), result.getDegree());
    }

    @Test
    void integrate() {
        firstPolynomial.addMonomial(new Monomial(3, 2));
        firstPolynomial.setDegree();

        Polynomial result = new Polynomial(new Monomial(1, 3));
        Polynomial integrate = PolyModel.integrate(firstPolynomial);

        assertEquals(integrate.getArrMonoms().get(0).getCoefficient(), result.getArrMonoms().get(0).getCoefficient());
        assertEquals(integrate.getArrMonoms().get(0).getExponent(), result.getArrMonoms().get(0).getExponent());
        assertEquals(integrate.getDegree(), result.getDegree());
    }
}