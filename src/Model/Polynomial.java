package Model;

import java.util.ArrayList;
import java.util.Comparator;

public class  Polynomial {
    private ArrayList<Monomial> arrMonoms;
    private int degree;

    public Polynomial(){
        arrMonoms = new ArrayList<Monomial>();
    }

    public Polynomial(Monomial m) {
        arrMonoms = new ArrayList<Monomial>();
        arrMonoms.add(m);
        setDegree();
    }

    public void setDegree() {
        if(!isZero())
            this.degree = arrMonoms.get(0).getExponent();
        else
            this.degree = -1;
    }

    public void addMonomial(Monomial monom) {
        arrMonoms.add(monom);
    }

    public void addPolynomial(ArrayList<Monomial> arrPolynomial) {
        for(Monomial m: arrPolynomial) {
            this.arrMonoms.add(m);
        }
        cleanupIdenticalExponents();
        if(!isZero())
            setDegree();
    }

    public ArrayList<Monomial> getArrMonoms() {
        return new ArrayList<>(arrMonoms);
    }

    public int getDegree() {
        return degree;
    }

    public void sortDescending() {
        this.arrMonoms.sort(new Comparator<Monomial>() {
            public int compare(Monomial o1, Monomial o2) {
                return o1.compareTo(o2);
            }
        });
    }

    public void cleanupIdenticalExponents() {
        this.sortDescending();
        int i = 0;
        int j;
        Monomial m;
        while(i <= this.arrMonoms.size()-2)
        {
            j = i+1;
            m = this.arrMonoms.get(i);
            while(j < this.arrMonoms.size() && m.getExponent() == this.arrMonoms.get(j).getExponent()) {//while the coeff of x is equal to the one next to it,
                m.setCoefficient(m.getCoefficient() + this.arrMonoms.get(j).getCoefficient());//it adds in the first monomial the coeff of the next ones which have the same degree
                this.arrMonoms.remove(j);
            }
            i++;
        }
        this.cleanZeroCoefficients();
    }

    public void cleanZeroCoefficients() {
        int i=0;
        Monomial m;
        while(i<this.arrMonoms.size()) {
            m = this.arrMonoms.get(i);
            if(m.getCoefficient() == 0 )
                this.arrMonoms.remove(i);
            else
                i++;
        }
    }

    public boolean isZero() {
        return arrMonoms.isEmpty();
    }

    public void negate() {
        for (Monomial n : this.arrMonoms) {
            n.negate();
        }
    }

    public String toString(){
        StringBuilder polynomialString = new StringBuilder();
        if      (this.getDegree() == -1 || isZero()) return "0";
        else if (this.getDegree() ==  0) return polynomialString.append(arrMonoms.get(0).getCoefficient()).toString();
        else if (this.getDegree() ==  1) {
            if(arrMonoms.get(0).getCoefficient() == 1)
                polynomialString.append("x");
            else if(arrMonoms.get(0).getCoefficient() == -1)
                polynomialString.append("-x");
            else
                polynomialString.append(arrMonoms.get(0).getCoefficient()).append("x");
            if(arrMonoms.size() == 2) {
                if (arrMonoms.get(1).getCoefficient() < 0)
                    polynomialString.append(arrMonoms.get(1).getCoefficient());
                else
                    polynomialString.append("+").append(arrMonoms.get(1).getCoefficient());
            }
            return polynomialString.toString();

        }
        for (Monomial n : this.arrMonoms) {
            polynomialString.append(monomToString(n));
        }
        System.out.println(polynomialString.toString());
        return polynomialString.toString();
    }

    public String monomToString(Monomial m) {
        if(m.getExponent() == 0) {
            if(m.getCoefficient() > 0)
                return new StringBuilder("+").append(m.getCoefficient()).toString();
            else
                return new StringBuilder("").append(m.getCoefficient()).toString();
        }
        else if(m.getExponent() == 1){
            if(m.getCoefficient() > 0) {
                if(m.getCoefficient() == 1)
                    return new StringBuilder("+").append("x").toString();
                else
                    return new StringBuilder("+").append(m.getCoefficient()).append("x").toString();
            }
            else {
                if(m.getCoefficient() == -1)
                    return new StringBuilder("-").append("x").toString();
                else
                    return new StringBuilder("").append(m.getCoefficient()).append("x").toString();
            }
        }else{
            if(m.getCoefficient() > 0) {
                if (m.getCoefficient() == 1)
                    return new StringBuilder("+").append("x^").append(m.getExponent()).toString();
                else
                    return new StringBuilder("+").append(m.getCoefficient()).append("x^").append(m.getExponent()).toString();
            }
            else {
                if (m.getCoefficient() == -1)
                    return new StringBuilder("-").append("x^").append(m.getExponent()).toString();
                else
                    return new StringBuilder("").append(m.getCoefficient()).append("x^").append(m.getExponent()).toString();
            }
        }
    }
}
