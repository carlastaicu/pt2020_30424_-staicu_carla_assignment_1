package Model;
public class PolyModel {
    private Polynomial firstPol;
    private Polynomial secondPol;
    private Polynomial resultingPol;
    private Polynomial divisionRemainderPol;

    public PolyModel() {
        reset();
    }

    public void reset() {
        firstPol = new Polynomial();
        secondPol = new Polynomial();
        resultingPol = new Polynomial();
        divisionRemainderPol = new Polynomial();
    }

    public void setPolynomials(String which, double coefficient, int exponent) {
        if(which.equals("first") == true) {
            firstPol.addMonomial(new Monomial(coefficient, exponent));
            firstPol.cleanupIdenticalExponents();
        } else {
            secondPol.addMonomial(new Monomial(coefficient, exponent));
            secondPol.cleanupIdenticalExponents();
        }
    }

    public Polynomial getFirstPol() {
        return this.firstPol;
    }

    public Polynomial getSecondPol() {
        return this.secondPol;
    }

    public Polynomial getResultingPol() {
        return resultingPol;
    }

    public Polynomial getDivisionRemainderPol() {
        return divisionRemainderPol;
    }

    public void setResultingPol(Polynomial resultingPol) {
        this.resultingPol = resultingPol;
    }

    public void setDivisionRemainderPol(Polynomial divisionRemainderPol) {
        this.divisionRemainderPol = divisionRemainderPol;
    }

    public static Polynomial addition(Polynomial a, Polynomial b) {
        Polynomial result = new Polynomial();
        result.addPolynomial(a.getArrMonoms());
        result.addPolynomial(b.getArrMonoms());

        return result;
    }

    public static Polynomial subtraction(Polynomial a, Polynomial b) {
        Polynomial result = new Polynomial();
        result.addPolynomial(a.getArrMonoms());
        b.negate();
        result.addPolynomial(b.getArrMonoms());

        return result;
    }

    public static Polynomial multiplication(Polynomial a, Polynomial b) {
        Polynomial result = new Polynomial();
        Monomial newM;
        double coefficient;
        int exponent;
        for(Monomial m: a.getArrMonoms()) {
            for(Monomial n: b.getArrMonoms()) {
                coefficient = m.getCoefficient() * n.getCoefficient();
                exponent = m.getExponent() + n.getExponent();
                newM = new Monomial(coefficient, exponent);
                result.addMonomial(newM);
            }
        }

        result.cleanupIdenticalExponents();
        if(!result.isZero())
            result.setDegree();
        return result;
    }

    public static Polynomial[] division(Polynomial a, Polynomial b) {
        Polynomial[] result = new Polynomial[2];
        result[0] = new Polynomial();

        Monomial firstMonom, newM;
        Polynomial newB, newA = a, newC;
        double coefficient;
        int exponent;
        while(!newA.isZero() && newA.getArrMonoms().get(0).getExponent() >= b.getArrMonoms().get(0).getExponent()) {
            firstMonom = newA.getArrMonoms().get(0);
            coefficient = firstMonom.getCoefficient()/b.getArrMonoms().get(0).getCoefficient();
            exponent = firstMonom.getExponent() - b.getArrMonoms().get(0).getExponent();

            newM = new Monomial(coefficient, exponent);
            newC = new Polynomial(newM);
            newB = multiplication(b, newC);
            newA = subtraction(newA, newB);
            result[0].addMonomial(newM);
        }
        if(!result[0].isZero())
            result[0].setDegree();
        result[1] = newA;
        if(!result[1].isZero())
            result[1].setDegree();
        return result;
    }

    public static Polynomial derive(Polynomial a) {
        Polynomial result = new Polynomial();
        Monomial newM;
        double coefficient;
        int exponent;
        for(Monomial m: a.getArrMonoms()) {
            if(m.getExponent() != 0) {
                coefficient = m.getCoefficient() * m.getExponent();
                exponent = m.getExponent() - 1;
                newM = new Monomial(coefficient, exponent);
                result.addMonomial(newM);
            }
        }

        result.cleanupIdenticalExponents();
        if(!result.isZero())
            result.setDegree();
        return result;
    }

    public static Polynomial integrate(Polynomial a) {
        Polynomial result = new Polynomial();
        Monomial newM;
        double coefficient;
        int exponent;
        for(Monomial m: a.getArrMonoms()) {
            coefficient = m.getCoefficient()/(m.getExponent()+1);
            exponent = m.getExponent() + 1;
            newM = new Monomial(coefficient, exponent);
            result.addMonomial(newM);
        }

        result.cleanupIdenticalExponents();
        if(!result.isZero())
            result.setDegree();
        return result;
    }
}
