package Model;

public class Monomial {
    private double coefficient;
    private int exponent;

    public Monomial(double coefficient, int exponent) {
        this.coefficient = coefficient;
        this.exponent = exponent;
    }

    public double getCoefficient() {
        return coefficient;
    }

    public int getExponent() {
        return exponent;
    }

    public void setCoefficient(double coefficient) {
        this.coefficient = coefficient;
    }

    public void setExponent(int exponent) {
        this.exponent = exponent;
    }

    public int compareTo(Monomial o) {
        if (this.exponent < o.exponent) {
            return 1;
        } else if(this.exponent>o.exponent) {
            return -1;
        } else if(this.exponent==o.exponent) {
            if (this.coefficient > o.coefficient)
                return -1;
            else if (this.coefficient < o.coefficient)
                return 1;
        }
        return 0;
    }

    public void negate() {
        this.coefficient = -coefficient;
    }
}
