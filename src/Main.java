import Controller.PolyController;
import Model.Monomial;
import Model.PolyModel;
import View.PolyView;
import Model.Polynomial;

public class Main {
    public static void main(String[] args)
    {
        PolyModel model = new PolyModel();
        PolyView view = new PolyView();
        new PolyController(model, view);
        view.setVisible(true);
    }
}
