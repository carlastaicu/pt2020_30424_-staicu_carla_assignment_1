package Controller;
import Model.PolyModel;
import Model.Polynomial;
import View.PolyView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PolyController {
    private PolyView m_view;
    private PolyModel m_model;

    public PolyController(PolyModel model, PolyView view) {
        m_model = model;
        m_view = view;

        view.addClearListener(new ClearListener());
        view.addAdditionListener(new AdditionListener());
        view.addSubtractListener(new SubtractListener());
        view.addMultiplyListener(new MultiplyListener());
        view.addDivideListener(new DivideListener());
        view.addDeriveListener(new DeriveListener());
        view.addIntegrateListener(new IntegrateListener());
    }

    public void getPolynomial(String which) {
        String input = (which.equals("first") ? m_view.getFirstUserInput() : m_view.getSecondUserInput());

        Pattern pattern = Pattern.compile("([+-]?[^-+]+)"); // -> for splitting
        Matcher matcher = pattern.matcher(input);

        Pattern mostComplex = Pattern.compile("^([-+])?(\\d+)x\\^(\\d+)$"); //-> makes 3 groups if -2x^3 -> group 1 = -, group 2 = 2, group 3 = 3
        Pattern mostComplex2 = Pattern.compile("^([+-])?x\\^(\\d+)$");// two  groups -> sign & exponent of x
        Pattern secondComplex = Pattern.compile("^([-+])?(\\d+)x$"); //makes two groups if -2x -> group 1 = -, group 2=2
        Pattern simpleX = Pattern.compile("^([+-])?(x)$"); // two groups -> sign &  x
        Pattern leastComplex = Pattern.compile("^([-+])?(\\d+)$"); //makes two groups, if -2 => group 1 = -, group 2 = 2
        Matcher matcher1, matcher2, matcher3, matcher4, matcher5;
        String component;
        double coefficient;
        int exponent;
        while (matcher.find()) {
            exponent = 1;
            coefficient = 1;
            component = matcher.group(1);

            matcher1 = mostComplex.matcher(component);
            matcher2 = mostComplex2.matcher(component);
            matcher3 = secondComplex.matcher(component);
            matcher4 = simpleX.matcher(component);
            matcher5 = leastComplex.matcher(component);

            try {
                if (matcher1.find()) {
                    coefficient = Double.parseDouble(matcher1.group(2));
                    exponent = Integer.parseInt(matcher1.group(3));

                    if(matcher1.group(1)!= null && matcher1.group(1).equals("-")) coefficient = -1 * coefficient;
                    m_model.setPolynomials(which, coefficient, exponent);
                } else if (matcher2.find()) {
                    exponent = Integer.parseInt(matcher2.group(2));

                    if(matcher2.group(1)!= null && matcher2.group(1).equals("-")) coefficient = -1;
                    m_model.setPolynomials(which, coefficient, exponent);
                } else if (matcher3.find()) {
                    coefficient = Double.parseDouble(matcher3.group(2));

                    if(matcher3.group(1)!= null && matcher3.group(1).equals("-")) coefficient = -1 * coefficient;
                    m_model.setPolynomials(which, coefficient, exponent);
                } else if (matcher4.find()) {
                    if(matcher4.group(1)!= null && matcher4.group(1).equals("-")) coefficient = -1 * coefficient;
                    m_model.setPolynomials(which, coefficient, exponent);
                } else if (matcher5.find()) {
                    coefficient = Double.parseDouble(matcher5.group(2));

                    if(matcher5.group(1)!= null && matcher5.group(1).equals("-")) coefficient = -1 * coefficient;
                    m_model.setPolynomials(which, coefficient, 0);

                } else {
                    throw new Exception(); //throw exception
                }
            }
            catch (Exception e){
                m_view.showError("INPUT POLYNOMIAL NOT VALID");
                m_view.reset();
                break;
            }
        }
    }

    class ClearListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            m_model.reset();
            m_view.reset();
        }
    }

    class AdditionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            m_model.reset();
            getPolynomial("first");
            getPolynomial("second");
            m_model.getFirstPol().toString();
            m_model.setResultingPol(m_model.addition(m_model.getFirstPol(), m_model.getSecondPol()));
            m_view.setTotal(m_model.getResultingPol().toString());
        }
    }

    class SubtractListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            m_model.reset();
            getPolynomial("first");
            getPolynomial("second");
            m_model.setResultingPol(m_model.subtraction(m_model.getFirstPol(), m_model.getSecondPol()));
            m_view.setTotal(m_model.getResultingPol().toString());
        }
    }

    class MultiplyListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            m_model.reset();
            getPolynomial("first");
            getPolynomial("second");
            m_model.setResultingPol(m_model.multiplication(m_model.getFirstPol(), m_model.getSecondPol()));
            m_view.setTotal(m_model.getResultingPol().toString());
        }
    }

    class DivideListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            m_model.reset();
            getPolynomial("first");
            getPolynomial("second");
            Polynomial[] result = m_model.division(m_model.getFirstPol(), m_model.getSecondPol());
            m_model.setResultingPol(result[0]);
            m_model.setDivisionRemainderPol(result[1]);
            m_view.setTotal(m_model.getResultingPol().toString());
            m_view.setRemainder(m_model.getDivisionRemainderPol().toString());
        }
    }

    class DeriveListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            m_model.reset();
            getPolynomial("first");
            m_model.setResultingPol(m_model.derive(m_model.getFirstPol()));
            m_view.setTotal(m_model.getResultingPol().toString());
        }
    }

    class IntegrateListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            m_model.reset();
            getPolynomial("first");
            m_model.setResultingPol(m_model.integrate(m_model.getFirstPol()));
            m_view.setTotal(m_model.getResultingPol().toString());
        }
    }
}
