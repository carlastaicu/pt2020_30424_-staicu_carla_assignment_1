package View;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class PolyView extends JFrame {
    private JTextField userInput = new JTextField(10);
    private JTextField userInputSecond = new JTextField(10);

    private JTextField result = new JTextField(20);
    private JTextField remainder = new JTextField(20);

    private JButton addButton = new JButton("Add");
    private JButton subtractButton = new JButton("Subtract");
    private JButton multiplyButton = new JButton("Multiply");
    private JButton divideButton = new JButton("Divide");
    private JButton deriveButton = new JButton("Derive");
    private JButton integrateButton = new JButton("Integrate");
    private JButton clearButton = new JButton("Clear");

    public PolyView() {
        result.setText(" ");
        result.setEditable(false);
        remainder.setText(" ");
        remainder.setEditable(false);

        JPanel content = new JPanel();
        content.setLayout(new FlowLayout());
        content.add(new JLabel("Polynomial 1"));
        content.add(userInput);
        content.add(new JLabel("Polynomial 2"));
        content.add(userInputSecond);

        content.add(addButton);
        content.add(subtractButton);
        content.add(multiplyButton);
        content.add(divideButton);
        content.add(deriveButton);
        content.add(integrateButton);

        content.add(new JLabel("Result"));
        result.setPreferredSize(new Dimension(150,75));
        content.add(result);

        content.add(new JLabel("Remainder"));
        remainder.setPreferredSize(new Dimension(50,25));
        content.add(remainder);
        content.add(clearButton);

        this.setContentPane(content);
        this.pack();

        this.setTitle("Simple Polynomial Calculator - MVC");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(900, 300);
    }

    public void reset() {
        userInput.setText(" ");
        userInputSecond.setText(" ");
        result.setText(" ");
        remainder.setText(" ");
    }

    public String getFirstUserInput() {
        return userInput.getText();
    }
    public String getSecondUserInput() {
        return userInputSecond.getText();
    }

    public void setTotal(String newTotal) {
        result.setText(newTotal);
    }
    public void setRemainder(String newRemainder) {
        remainder.setText(newRemainder);
    }

    public void showError(String errMessage) {
        JOptionPane.showMessageDialog(this, errMessage);
        reset();
    }

    public void addClearListener(ActionListener cal) {
        clearButton.addActionListener(cal);
    }

    public void addAdditionListener(ActionListener mal) {
        addButton.addActionListener(mal);
    }

    public void addSubtractListener(ActionListener mal) {
        subtractButton.addActionListener(mal);
    }

    public void addMultiplyListener(ActionListener mal) {
        multiplyButton.addActionListener(mal);
    }

    public void addDivideListener(ActionListener mal) {
        divideButton.addActionListener(mal);
    }

    public void addDeriveListener(ActionListener mal) {
        deriveButton.addActionListener(mal);
    }

    public void addIntegrateListener(ActionListener mal) {
        integrateButton.addActionListener(mal);
    }

}
